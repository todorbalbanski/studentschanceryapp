import QtQuick 2.2
import QtQuick.Controls 1.1


Rectangle {
    property alias vAddressDisplayId: addressDisplayId
    property alias vFaknumberDisplayId: faknumberDisplayId
    property alias vnameDisplayId: nameDisplayId
    property alias vfamilyNameDisplayId: familynameDisplayId
    property alias vAvatarDisplayId: avatarId
    property alias vrootId: rootId
    //property alias vIndex: index

    id: rootId
    property bool checked: false
    width: 180
    height: 180
    color: "transparent"

    QtObject {
        id:  qmlObjectId
        property int choosedIndex: index
    }

    Text {
        id: addressDisplayId
        x: 11
        y: 100
        width: 67
        height: 39
        text: address//qsTr("Address")
        font.pixelSize: 12
    }

    Text {
        id: faknumberDisplayId
        x: 11
        y: 7
        width: 106
        height: 14
        text: "<b>FN:</b> " + faknumber// qsTr("Text")
        font.pixelSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: nameDisplayId
        x: 11
        y: 33
        width: 63
        height: 14
        text: name// qsTr("Text")
        font.pixelSize: 12
    }

    Text {
        id: familynameDisplayId
        x: 11
        y: 63
        width: 66
        height: 14
        text: familyname//qsTr("Text")
        font.pixelSize: 12
        //onTextChanged:setColorBy()
    }

    Image {
        id: avatarId
        x: 99
        y: 21
        width: 100
        height: 100
        source: "qrc:/img/img/" + avatar
    }
     MouseArea {
         id: mouse_area1
         anchors.fill: parent
         z: 1
         hoverEnabled: false

         onClicked: {
             var indexTo = index
             print(indexTo);
             //console.log(qmlObjectId.choosedIndex)
             var sig = indexTo.toString();
            // guiSignal(qmlObjectId);
             gridView.currentIndex = index;
             //selectionId.choosedRow = index;
             //console.log("hey" +qmlObjectId.choosedRow)
            // itemId.selectItem(index);
             selectionModel.setSelectionChanged(index);

         }
     }

}
