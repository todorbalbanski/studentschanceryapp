#-------------------------------------------------
#
# Project created by QtCreator 2017-05-10T11:28:04
#
#-------------------------------------------------

QT       += core gui sql xml network quick qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StudentsChanceryApp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
           View/mainview2.cpp \
           View/newstudent2.cpp \
    connection.cpp \
    Data/student.cpp \
    Serializers/baseserializer.cpp \
    Serializers/xmlserializer.cpp \
    Serializers/sqlserializer.cpp \
    Serializers/sermng.cpp \
    Model/studentmodelclass.cpp \
    View/loadstudentsfrom.cpp \
    Serializers/viewmanager.cpp \
    Serializers/downloadmanager.cpp \
    Model/proxymodelforfiltering.cpp \
    Model/studentsselectionmodel.cpp

HEADERS += \
           View/mainview2.h \
           View/newstudent2.h \
    connection.h \
    Data/student.h \
    Serializers/baseserializer.h \
    Serializers/xmlserializer.h \
    Serializers/sqlserializer.h \
    Serializers/sermng.h \
    Model/studentmodelclass.h \
    View/loadstudentsfrom.h \
    Serializers/viewmanager.h \
    Serializers/downloadmanager.h \
    Model/proxymodelforfiltering.h \
    Model/studentsselectionmodel.h

FORMS   += View/mainview.ui \
           View/newstudent.ui

RESOURCES += \
           images.qrc

DISTFILES += \
    img/student1.jpg \
    img/student2.jpg \
    img/student3.jpg \
    img/student4.jpg \
    img/student5.png \
    studentsGrid.qml \
    StudentsField.qml
