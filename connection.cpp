#include "connection.h"
#include <QApplication>
#include <QStandardPaths>


QSqlDatabase Connection::studentsDB = QSqlDatabase::addDatabase("QSQLITE");
bool Connection::isConnOpen = false;

Connection::Connection()
{
    //studentsDB = QSqlDatabase::addDatabase("QSQLITE");

}

void Connection::inializeDB(QString aDbPath)
{
    //studentsDB.setDatabaseName( "C:/ProgramData/StudentsChanceryApp/config/Students.db" );
    //QString dbPath = QString("%PROGRAMDATA%%1%2%1config%1Students.db" ).arg( QDir::separator() ).arg( QApplication::applicationName() ).arg("config").arg("Students.db");
    studentsDB.setDatabaseName(aDbPath);
}

void Connection::destructDB()
{
    if(studentsDB.isOpen())
    {
        studentsDB.close();
    }
    studentsDB.removeDatabase(QSqlDatabase::defaultConnection);
}

void Connection::connClose()
{
    studentsDB.close();
    //studentsDB.removeDatabase(QSqlDatabase::defaultConnection);
}


 bool Connection::connOpen()
{
    //const QString dbFilePath = QString("%PROGRAMDATA%%1%2%1config%1Students.db" ).arg( QDir::separator() ).arg( QApplication::applicationName() );
    studentsDB.open();
    if(studentsDB.isOpen())
    {
        isConnOpen = true;
    }else{
        isConnOpen = false;
    }
    return isConnOpen;
 }

 QSqlDatabase Connection::GetDbInstance()
 {
     return studentsDB;
 }
