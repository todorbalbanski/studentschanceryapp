#include "downloadmanager.h"
#include <QDir>

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{
    connect(&m_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(downloadFinished(QNetworkReply*)));
}

void DownloadManager::executeDownload(QString aStudentsUrl)
{
    QTimer::singleShot(0, this, SLOT(execute(studentsUrl)));
}

void DownloadManager::download(QUrl aUrl)
{
    doDownload(aUrl);
}

void DownloadManager::doDownload(const QUrl &url)
{
    QNetworkRequest request(url);
        QNetworkReply *reply = m_manager.get(request);

        m_currentDownloads.append(reply);
}

QString DownloadManager::saveFileName(const QUrl &url)
{
    QString path = url.path();
        QString basename = QFileInfo(path).fileName();

        //if (basename.isEmpty())
            basename = "StudentsFromWeb";
            QString checkIfBaseNameExists = basename + ".xml";

        if (QFile::exists(checkIfBaseNameExists)) {
            // already exists, don't overwrite
            int i = 0;
            basename += '.';
            while (QFile::exists(basename + QString::number(i)))
                ++i;

            basename += QString::number(i);
        }
        basename += ".xml";

        return basename;
}

bool DownloadManager::saveToDisk(const QString &filename, QIODevice *data)
{
    QString outputDir = "C:/ProgramData/StudentsChanceryApp/config"; // with "/" separators
    QString fileOut = outputDir+ "/" +filename;

    QFile file(fileOut);
        if (!file.open(QIODevice::WriteOnly)) {
            fprintf(stderr, "Could not open %s for writing: %s\n",
                    qPrintable(fileOut),
                    qPrintable(file.errorString()));
            return false;
        }

        file.write(data->readAll());
        file.close();
        emit NotifyWhenFileIsDownloaded(fileOut);

        return true;
}

void DownloadManager::execute()
{
    QUrl url = QUrl::fromEncoded(m_urlPath.toLocal8Bit());
    doDownload(url);
}

void DownloadManager::setUrlPath(QString aUrlPath)
{
    m_urlPath = aUrlPath;
}

void DownloadManager::downloadFinished(QNetworkReply *reply)
{
    QUrl url = reply->url();
        if (reply->error()) {
            fprintf(stderr, "Download of %s failed: %s\n",
                    url.toEncoded().constData(),
                    qPrintable(reply->errorString()));
        } else {
            QString filename = saveFileName(url);
            if (saveToDisk(filename, reply))
                printf("Download of %s succeeded (saved to %s)\n",
                       url.toEncoded().constData(), qPrintable(filename));
        }

        m_currentDownloads.removeAll(reply);
        reply->deleteLater();
}
