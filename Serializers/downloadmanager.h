#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QTimer>
#include <QUrl>

#include <stdio.h>

//FIXME:  Да почиста публичния интерфейс на клас и да адна нови функции - 5 public, 2 signala progres, finish
class DownloadManager : public QObject
{
    Q_OBJECT

private:
    QNetworkAccessManager m_manager;
    QList<QNetworkReply *> m_currentDownloads;
    QString m_urlPath;

public:
    explicit DownloadManager(QObject *parent);
    void doDownload(const QUrl &url);
    QString saveFileName(const QUrl &url);

private:
    bool saveToDisk(const QString &filename, QIODevice *data);
    void download(QUrl aUrl);

signals:
    void NotifyWhenFileIsDownloaded(QString aFileName);

public slots:
    void execute();
    void setUrlPath(QString aUrlPath);
    void executeDownload(QString aStudentsUrl);
    void downloadFinished(QNetworkReply *reply);
};

#endif // DOWNLOADMANAGER_H
