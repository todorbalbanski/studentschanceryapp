#ifndef BASESERIALIZER_H
#define BASESERIALIZER_H

#include "data/student.h"

#include <QString>
#include <QList>

class BaseSerializer
{
public:
    virtual bool writeStudents( const QList<Student>& aStudent, const QString& aFilePath ) = 0;
    virtual QList<Student> readStudents( const QString& aFilePath ) = 0;
};

#endif // BASESERIALIZER_H
