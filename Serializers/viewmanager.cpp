#include "viewmanager.h"
#include "Serializers/sermng.h"
#include "View/mainview2.h"
#include "Model/studentmodelclass.h"

ViewManager::ViewManager()
    : m_mainview(Q_NULLPTR), m_serMng(Q_NULLPTR), m_dwnManager(Q_NULLPTR)
{
    this->SetupConnection();
//    m_quickview.setResizeMode(QQuickView::SizeRootObjectToView);
//    QQmlContext *ctxt = m_quickview.rootContext();
//    qDebug()<< ctxt;
  // m_quickview.setSource(QUrl("StudentsQml.qml"));
  // m_quickview.show();
}

void ViewManager::SetupConnection()
{
    connect(& m_serMng, SIGNAL(NotifyUsersAreLoaded(const QList<Student>&)), & m_mainview, SLOT(loadStudents(const QList<Student>&)) );
    connect(& m_mainview, SIGNAL(saveStudentsSignal(QList<Student>,QString,QString)), &m_serMng, SLOT(saveStudents(QList<Student>,QString,QString)));
    connect(& m_mainview, SIGNAL(readStudentsFromFile(QString,QString)), &m_serMng, SLOT(readStudents(QString,QString)));
    connect(& m_mainview, SIGNAL(setUrlPathToDwnMng(QString)), & m_dwnManager, SLOT(setUrlPath(QString)));
    connect(& m_dwnManager, SIGNAL(NotifyWhenFileIsDownloaded(QString)), &m_mainview, SLOT(getStudentsDownloadedFromWeb(QString)));
    connect(& m_mainview, SIGNAL(ExecuteDownloading(QString)), &m_dwnManager, SLOT(executeDownload(QString)));
}

void ViewManager::show()
{
    m_mainview.show();
}
