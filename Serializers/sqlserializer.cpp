  #include "sqlserializer.h"

SqlSerializer::SqlSerializer()
{

}

bool SqlSerializer::writeStudents(const QList<Student> &aStudents, const QString &aFilePath)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(aFilePath);
    if ( ! db.open() )
    {
        return false;
    }

    QSqlQuery q = db.exec( "SELECT COUNT(*) FROM Students" );
    if ( ! q.isValid() )
    {
        const QVariant studentsCount = q.record().value( 0 );
        qDebug() << "students count = " << studentsCount.value< int >();
    }

    QSqlQuery query;
    const bool wasTableCreated = query.exec("create table Students (ID INTEGER PRIMARY KEY   AUTOINCREMENT, NAME text, FamilyName text, FakNumber INTEGER, AGE int, Address varchar(50), Avatar varchar(100))");
    for (int i = 0; i < aStudents.count(); ++i) {
        bool y = SaveStudent(aStudents[i].name(), aStudents[i].familyName(), aStudents[i].age(),aStudents[i].address(), aStudents[i].faknumber(), aStudents[i].avatar());
        if(y == false)
        {
            return false;
        }
    }
    return false;
}

QList<Student> SqlSerializer::readStudents(const QString &aXmlFilePath)
{
    Connection::inializeDB(aXmlFilePath);
    Connection::connOpen();

    QSqlDatabase dbInstance = Connection::GetDbInstance();
    if(dbInstance.isOpen())
    {
        qDebug()<<"Failed to Open the DB!";
    }
    if(dbInstance.open())
    {
        qDebug()<<"Connected!";
    }else{
    }

    QSqlQuery query;
    QString queryString = "SELECT * FROM STUDENTS";

    query.exec(queryString);

    int c = 0;
    QList<Student> m_studentsList;

    while (query.next()) {
        Student student;
        QSqlRecord record = query.record();
        student.setId(record.value("id").toInt());
        student.setName(record.value("name").toString());
        student.setFamilyName(record.value("familyname").toString());
        student.setFakNumber(record.value("faknumber").toLongLong());
        student.setAge(record.value("age").toInt());
        student.setAddress(record.value("address").toString());
        student.setAvatar(record.value("avatar").toString());
        m_studentsList.append(student);
        qDebug() << "Date : " << record.value("name");
        c++;
    }

    return m_studentsList;
}

bool SqlSerializer::SaveStudent(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber, QString aAvatar)
{
    Student student;
    student.setName(aName);
    student.setFamilyName(aFamilyName);
    student.setFakNumber(aFakNumber);
    student.setAddress(aAddress);
    student.setAge(aAge);
    student.setAvatar(aAvatar);

    QString FakNumberString = QString::number(student.faknumber());
    QString AgeString = QString::number(aAge);
    QSqlQuery qry;
    qry.prepare("insert into Students(name, familyName, fakNumber, age, address, avatar) values ('"+student.name()+"', '"+student.familyName()+"', '"+FakNumberString+"','"+AgeString+"','"+student.address()+"', '"+student.avatar()+"')");

    if(qry.exec())
    {
        return true;

    }else{
        return false;
    }

}
