#include "sermng.h"

#include "Serializers/xmlserializer.h"
#include "Serializers/sqlserializer.h"

#include <map>

using namespace std;

Q_DECLARE_METATYPE(QList<Student>)
const int studentsListMetatypeId = qRegisterMetaType< QList<Student> >();
Q_DECLARE_METATYPE(Student)
const int studentMetatypeId = qRegisterMetaType< Student >();

SerMng::SerMng(QObject *parent)
{
}

enum StudentsExtension { xml, db};
map<QString, StudentsExtension> studentsPaths;
bool isXmlDbMapRegistered = false;


void SerMng::registerFilePaths(){
    studentsPaths["xml"] = xml;
    studentsPaths["db"] = db;
    isXmlDbMapRegistered = true;
}


void SerMng::readStudents(QString aFilePath, QString aFileExtension)
{
    registerFilePaths();
    QScopedPointer< BaseSerializer > baseSerializer;
    QList<Student> listStudents;

    switch( studentsPaths[aFileExtension] )
    {
        case xml:
        {
            baseSerializer.reset( new XmlSerializer );
            break;
        }
        case db:
        {
            baseSerializer.reset( new SqlSerializer );
            break;
        }
    }

    if ( baseSerializer.isNull() )
    {
        return;
    }

    listStudents = baseSerializer->readStudents(aFilePath);
    emit NotifyUsersAreLoaded(listStudents);
}

bool SerMng::saveStudents(QList<Student> aStudentList, QString aFilePath, QString aFileExtension)
{
    if(!isXmlDbMapRegistered)
    {
        registerFilePaths();
    }

    BaseSerializer* baseSerializer;
    switch( studentsPaths[aFileExtension] )
    {
        case xml:
        {
            baseSerializer = new XmlSerializer;
            return baseSerializer->writeStudents(aStudentList, aFilePath);
        }
        case db:
        {
            baseSerializer = new SqlSerializer;
            return baseSerializer->writeStudents(aStudentList, aFilePath);
        }
    }

    return false;
}
