#ifndef SERMNG_H
#define SERMNG_H

#include <QList>
#include <QString>

#include <vector>
#include <sstream>

#include "Data/student.h"

enum WebOrLocalToLoadStudents
{
    FromWeb,
    FromLocal
};

class SerMng : public QObject
{
    Q_OBJECT

public:
    SerMng(QObject *parent);

private:
    void registerFilePaths();

public slots:
    bool saveStudents(QList<Student> aStudentList, QString aFilePath, QString aFileExtension);
    void readStudents(QString aFilePath, QString aFileExtension);

signals:
    void NotifyUsersAreLoaded( const QList<Student>& aListStudnets);
};

#endif // SERMNG_H
