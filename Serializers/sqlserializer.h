#ifndef SQLSERIALIZER_H
#define SQLSERIALIZER_H

#include "Serializers/baseserializer.h"
#include <list>
#include <QList>
#include <QSqlRecord>

class SqlSerializer : public BaseSerializer
{
public:
    SqlSerializer();

    QList<Student> readStudents(const QString& aFilePath) Q_DECL_OVERRIDE;
    bool writeStudents( const QList<Student>& aStudents, const QString& aFilePath ) Q_DECL_OVERRIDE;
    bool SaveStudent(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber, QString aAvatar);
};

#endif // SQLSERIALIZER_H
