#ifndef XMLSERIALIZER_H
#define XMLSERIALIZER_H

#include "Serializers/baseserializer.h"
#include <QXmlStreamReader>
class XmlSerializer : public BaseSerializer
{
public:
    XmlSerializer();

    bool writeStudents( const QList<Student>& aStudentList, const QString& aFilePath );
    QList<Student> readStudents(const QString& aFilePath);

private:
    QString GetXmlElementContent(QDomElement root, QString tagname);
};

#endif // XMLSERIALIZER_H
