#ifndef VIEWMANAGER_H
#define VIEWMANAGER_H

#include "Serializers/sermng.h"
#include "View/mainview2.h"
#include "Serializers/downloadmanager.h"
#include "View/loadstudentsfrom.h"
#include "Serializers/sermng.h"
#include <QQuickView>
#include <QUrl>

#include <QObject>

class ViewManager : public QObject
{
    Q_OBJECT
public:
    explicit ViewManager();
    void show();

private:
    void SetupConnection();

private:
    SerMng m_serMng;
    mainview2 m_mainview;
    DownloadManager m_dwnManager;
    //QQuickView m_quickview;
};

#endif // VIEWMANAGER_H
