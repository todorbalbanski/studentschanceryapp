#include "xmlserializer.h"
#include <QXmlStreamReader>
#include <QMessageBox>

XmlSerializer::XmlSerializer()
{

}

bool XmlSerializer::writeStudents(const QList<Student>& aStudentList, const QString &aFilePath)
{
    //Create XmlDocument
    QDomDocument document;

    //Make the root Element
    //if(document)
    QDomElement root = document.createElement("students");

    //Add it to the document
    document.appendChild(root);

    for(int i=0; i< aStudentList.count() ;i++)
    {
        QString currentStudentID = QString::number(aStudentList[i].id());
        QString currentStudentName = aStudentList[i].name();
        QString currentStudentFamilyName = aStudentList[i].familyName();
        QString currentStudentFaknumber = QString::number(aStudentList[i].faknumber());
        QString currentStudentAge = QString::number(aStudentList[i].age());
        QString currentStudentAddress = aStudentList[i].address();
        QString currentStudentAvatar = aStudentList[i].avatar();

        QDomElement student = document.createElement("student");
        QDomElement id = document.createElement("id");
        QDomText idText = document.createTextNode(currentStudentID);
        id.appendChild(idText);
        student.appendChild(id);

        QDomElement name = document.createElement("name");
        QDomText nameText = document.createTextNode(currentStudentName);
        name.appendChild(nameText);
        student.appendChild(name);

        QDomElement familyname = document.createElement("familyname");
        QDomText familyNameText = document.createTextNode(currentStudentFamilyName);
        familyname.appendChild(familyNameText);
        student.appendChild(familyname);

        QDomElement faknumber = document.createElement("faknumber");
        QDomText faknumberText = document.createTextNode(currentStudentFaknumber);
        faknumber.appendChild(faknumberText);
        student.appendChild(faknumber);

        QDomElement age = document.createElement("age");
        QDomText ageText = document.createTextNode(currentStudentAge);
        age.appendChild(ageText);
        student.appendChild(age);

        QDomElement address = document.createElement("address");
        QDomText addressText = document.createTextNode(currentStudentAddress);
        address.appendChild(addressText);
        student.appendChild(address);

        QDomElement avatar = document.createElement("avatar");
        QDomText avatarText = document.createTextNode(currentStudentAvatar);
        avatar.appendChild(avatarText);
        student.appendChild(address);

        root.appendChild(student);

    }

    //Write to file
    //QFile file( QStandardPaths::writableLocation( QStandardPaths::DesktopLocation ) % QDir::separator() % "StudentsFromDB.xml" );
    QFile file( aFilePath );
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Failed to Open the file";
        return false;
    }else{
        QTextStream stream(&file);
        stream << document.toString();
        file.close();
        return true;
    }
}


QList<Student> XmlSerializer::readStudents( const QString& aFilePath)
{
    QDomDocument document;
    QFile xmlFile(aFilePath);
    if(!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() <<"Failed to open file";
    }
    else
    {
        if(!document.setContent(&xmlFile))
        {
            qDebug() <<"failed";
        }

        xmlFile.close();
    }

    //RootElement
    QDomElement root = document.firstChildElement();
    QDomNodeList students = root.elementsByTagName("student");

    Student currentStudent;
    QList< Student > studentsList;
    for (int var = 0; var < students.count(); ++var)
    {
        QDomNode itemnode = students.at(var);

        if ( itemnode.isElement() )
        {
            QDomElement student = itemnode.toElement();
            currentStudent.setId(GetXmlElementContent(student, "id").toInt());
            currentStudent.setName(GetXmlElementContent(student, "name"));
            currentStudent.setFamilyName(GetXmlElementContent(student, "familyname"));
            currentStudent.setFakNumber(GetXmlElementContent(student, "faknumber").toLong());
            currentStudent.setAge(GetXmlElementContent(student, "age").toInt());
            currentStudent.setAddress(GetXmlElementContent(student, "address"));
            currentStudent.setAvatar(GetXmlElementContent(student, "avatar"));
        }

        qDebug() << "avatar:" <<currentStudent.avatar();
        studentsList.append(currentStudent);
    }

    return studentsList;
}

QString XmlSerializer::GetXmlElementContent(QDomElement root, QString tagname)
{
    QDomNodeList items = root.elementsByTagName(tagname);

    int itemsCount = items.count();
    for (int var = 0; var < itemsCount; ++var) {
        QDomNode itemnode = items.at(var);
        if(itemnode.isElement())
        {
            QDomElement studentSearchedValue = itemnode.toElement();
            //qDebug()<< studentSearchedValue.text();
            qDebug() << studentSearchedValue.text();
            return studentSearchedValue.text();
        }
    }
    return "";
}
