import QtQuick 2.4

Item{
    width: 600
    height: 600
    property alias gridView: gridView
    scale: 1.1
    opacity: 1

    GridView {
        id: gridView
        x: 67
        y: 43
        width: 402
        height: 524
        model: myModel
        cellHeight: 200
        cellWidth: 200
        delegate: Item {
            x: 5
            height: 50
            Column {
                spacing: 20
                Rectangle {
                    id: rect1
                    width: 200
                    height: 200
                    color: ListView.isCurrentItem?"#157efb":"#53d769"

                    focus: true
                    anchors.horizontalCenter: parent.horizontalCenter
                                       Text {
                                           id: addressDisplay
                                           x: 11
                                           y: 100
                                           width: 67
                                           height: 39
                                           text: address//qsTr("Address")
                                           font.pixelSize: 12
                                       }

                                       Text {
                                           id: faknumberDisplay
                                           x: 11
                                           y: 7
                                           width: 106
                                           height: 14
                                           text: "<b>FN:</b> " + faknumber// qsTr("Text")
                                           font.pixelSize: 12
                                           anchors.horizontalCenter: parent.horizontalCenter
                                       }

                                       Text {
                                           id: nameDisplay
                                           x: 11
                                           y: 33
                                           width: 63
                                           height: 14
                                           text: name// qsTr("Text")
                                           font.pixelSize: 12
                                       }

                                       Text {
                                           id: familynameDisplay
                                           x: 11
                                           y: 63
                                           width: 66
                                           height: 14
                                           text: familyname//qsTr("Text")
                                           font.pixelSize: 12
                                       }

                                       Image {
                                           id: image
                                           x: 99
                                           y: 21
                                           width: 100
                                           height: 100
                                           source: "qrc:/img/img/" + avatar
                                       }
                }

//                Text {
//                    x: 5
//                    text: name
//                    font.bold: true
//                    anchors.horizontalCenter: parent.horizontalCenter
//                }
            }

        }
        highlight: highlight
        highlightFollowsCurrentItem: true
        focus: true
    }

}
