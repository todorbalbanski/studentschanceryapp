#ifndef CONNECTION_H
#define CONNECTION_H

#include <QSqlDatabase>
#include <QDir>
#include <shlobj.h>

 class Connection
{
public:

    Connection();
private:
    static QSqlDatabase studentsDB;


//methods
 public:
    static bool isConnOpen;
    static void inializeDB(QString aDbPath);
    static void destructDB();
    static void connClose();
    static bool connOpen();
    static QSqlDatabase GetDbInstance();
};

#endif // CONNECTION_H
