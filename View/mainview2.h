#ifndef MAINVIEW2_H
#define MAINVIEW2_H

#include <QMainWindow>
#include <QtDebug>
#include <QFileInfo>
#include <QtSql>
#include <QList>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTableView>
#include <QToolButton>
#include <QMenuBar>
#include <QFileDialog>
#include <QContextMenuEvent>
#include <QRadioButton>
#include <QLineEdit>
#include <QGroupBox>
#include <QSortFilterProxyModel>

#include "Model/proxymodelforfiltering.h"
#include "loadstudentsfrom.h"

#include "Model/studentsselectionmodel.h"


#include <QQuickView>
#include <QUrl>

class NewStudent2;
class StudentsModel;

class mainview2 : public QMainWindow
{
    Q_OBJECT

public:
    explicit mainview2(QWidget *parent);
    ~mainview2();

    //QFileDialog getOpenFileName;
private:
    void createActions();
    void createMenus();
    void populateTableViewFromList( const QList<Student>& studentsList );
    void loadStudentsLocal();
    void setLayout();
    void getTableToList();
    void setQML();
    void setConnections();
    void createTables();
private:
    //QHBoxLayout *hLayout;
    QVBoxLayout* m_mainLayout;
    QHBoxLayout* hLayout;
    QPushButton *m_buttonLoadTable;
    QTableView *m_tableview;
    QTableView *m_tableview2;

    //QHBoxLayout *vLayout;
    //QHBoxLayout *vLayoutUpper;
    QToolButton *m_buttonAddStudent;
    QToolButton *m_buttonRemoveStudent;

    QWidget *w;

    QAction *m_LoadStudents;
    QAction *m_SaveStudents;
    QAction *m_ExitAct;

    //Lists
    QString m_currentStudentId;

    StudentsSelectionModel* m_selectionModel;
    StudentsModel* m_model;
    Student* m_student;
    NewStudent2* m_studentEditor;
    QQuickView *m_quickview;


    ProxyModelForFiltering* proxyModel;

    //LoadStudentsFrom& m_loadStudents;

    QGroupBox* m_groupBox;
    QRadioButton* m_radio1;
    QRadioButton* m_radio2;
    QLineEdit* m_searchInput;

protected:
#ifndef QT_NO_CONTEXTMENU
    void contextMenuEvent(QContextMenuEvent *event) override;
#endif // QT_NO_CONTEXTMENU

signals:
    void readStudentsFromFile(QString,QString);
    bool saveStudentsSignal(QList<Student> aStudentList, QString aFilePath, QString aFileExtension);
    void setUrlPathToDwnMng(QString);
    void DwnManagerNotifiedFileIsDownloaded(QString);
    void ExecuteDownloading(QString aStudentsUrl);
    void notifySelectedRowChanged(int aRow);


public slots:
    void loadStudents(const QList<Student>& aStudentList);
    void readStudents(QString aFileName, QString aFileExtension);
    void setUrlPathToDwnManager(QString aUrlPath);
    void getStudentsDownloadedFromWeb(QString aFileName);
    void executeDownload(QString studentsUrl);
    void getSelectedItem(int index);
    void tableSelectionStudentChanged(QModelIndex x, QModelIndex y);
    void onGuiSignal(const QVariant& qmlObject);


private slots:
    void on_btnAddStudent_clicked();
    void on_btnRemoveStudent_clicked();
    void on_tableView_activated(const QModelIndex &index);

    //menu
    void loadStudentsWindow();
    void saveStudents();
    void tableViewSelectionChangedSlot(QModelIndex curIndex, QModelIndex prevIndex);
    void tableView2SelectionChangedSlot(QModelIndex curIndex, QModelIndex prevIndex);

    //Search Slot
    void FilterTable(const QString &aText);
};



#endif // MAINVIEW2_H
