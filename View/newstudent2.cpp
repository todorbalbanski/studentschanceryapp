#include "newstudent2.h"

#include <QStringBuilder>
#include <QStandardPaths>
#include <QRegExpValidator>
#include <QToolTip>

NewStudent2::NewStudent2( QWidget *parent, Qt::WindowFlags f )
    : QDialog( parent, f )
{
    //buttons
    btnSaveStudent = new QToolButton(nullptr);
    btnSaveStudent->setIcon(QIcon(":/img/img/save.png"));
    btnSaveStudent->setIconSize(QSize(46,46));
    btnSaveStudent->setDisabled(true);
    connect(btnSaveStudent, SIGNAL(clicked()), this, SLOT(on_btnSaveStudent_clicked()));

    btnBack = new QToolButton(nullptr);
    btnBack->setIcon(QIcon(":/img/img/back.png"));
    btnBack->setIconSize(QSize(46,46));
    connect(btnBack, SIGNAL(clicked()), this, SLOT(on_btnBack_clicked()));

    labelName = new QLabel("Name");
    lineEditName = new QLineEdit();
    lineEditName->setValidator(new QRegExpValidator(QRegExp("[A-z]{3,15}")));
    connect( lineEditName, SIGNAL( textChanged( QString ) ), this, SLOT(  on_lineEditName_text_changed( QString ) ) );

    labelFamilyName = new QLabel("FamilyName");
    lineEditFamilyName = new QLineEdit();
    lineEditFamilyName->setValidator(new QRegExpValidator(QRegExp("[A-z]{3,15}")));
    connect( lineEditFamilyName, SIGNAL( textChanged( QString ) ), this, SLOT(  on_lineEditFamilyName_text_changed( QString ) ) );

    labelAge = new QLabel("Age");
    lineEditAge = new QLineEdit();
    lineEditAge->setValidator(new QRegExpValidator(QRegExp("[0-9]|[0-9][0-9]|[0-9][0-9][0-9]")));
    connect( lineEditAge, SIGNAL( textChanged( QString ) ), this, SLOT( on_lineEditAge_text_changed( QString ) ) );

    labelAddress = new QLabel("Address");
    lineEditAddress = new QLineEdit();

    labelFakNumber = new QLabel("FakNumber");
    lineEditFakNumber = new QLineEdit();
    lineEditFakNumber->setValidator(new QRegExpValidator(QRegExp("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")));
    connect( lineEditFakNumber, SIGNAL( textChanged( QString ) ), this, SLOT( on_lineEditFakNumber_text_changed( QString ) ) );

    setlayots();

    // Window title
    this->setWindowTitle("Student Chancery");
}

void NewStudent2::setlayots()
{
    // Horizontal layout with 3 buttons
    hLayout = new QHBoxLayout;
    // Vertical layout with 2 buttons
    vLayout = new QVBoxLayout;
    //Add to the layout

    hLayout->addWidget(btnSaveStudent);
    hLayout->addWidget(btnBack);
    hLayout->addStretch(20);

    // Outer Layer
    mainLayout = new QVBoxLayout;


    vLayout->addWidget(labelName);
    vLayout->addWidget(lineEditName);
    vLayout->addWidget(labelFamilyName);
    vLayout->addWidget(lineEditFamilyName);
    vLayout->addWidget(labelAge);
    vLayout->addWidget(lineEditAge);
    vLayout->addWidget(labelAddress);
    vLayout->addWidget(lineEditAddress);
    vLayout->addWidget(labelFakNumber);
    vLayout->addWidget(lineEditFakNumber);

    // Add the previous two inner layouts
    mainLayout->addLayout(hLayout);
    mainLayout->addLayout(vLayout);

    this->setLayout(mainLayout);
}


void NewStudent2::on_btnSaveStudent_clicked()
{
    /*
    Connection::connOpen();
    bool isExecuted = student->SaveStudent(lineEditName->text(), lineEditFamilyName->text(), lineEditAge->text().toInt(), lineEditAddress->text(), lineEditFakNumber->text().toLong());
    if(isExecuted)
    {
        QMessageBox::information(this, tr("Save!"), tr("Student added!"));
    }else{
        QMessageBox::critical(this, tr("Inventaire!"), tr("Error!"));
    }
    */

}

void NewStudent2::on_btnBack_clicked()
{
    this->close();
}

void NewStudent2::on_lineEditFakNumber_text_changed(QString text)
{

        if(lineEditFakNumber->hasAcceptableInput() == false)
        {
            QToolTip::showText(lineEditFakNumber->mapToGlobal(QPoint()), tr("Please Input a valid FakNumber"));
        }
        else
        {
            //QToolTip::showText(lineEditFakNumber->mapToGlobal(QPoint()), tr("It's Valid!"));
            QToolTip::hideText();
        }
        isModelRelevant();

}

void NewStudent2::on_lineEditAge_text_changed(QString)
{
    if(lineEditAge->hasAcceptableInput() == false)
    {
        QToolTip::showText(lineEditAge->mapToGlobal(QPoint()), tr("Please Input a valid Age"));
    }
    else
    {
        //QToolTip::showText(lineEditFakNumber->mapToGlobal(QPoint()), tr("It's Valid!"));
        QToolTip::hideText();
    }
    isModelRelevant();
}

void NewStudent2::on_lineEditFamilyName_text_changed(QString)
{
    if(!lineEditFamilyName->hasAcceptableInput())
    {
        QToolTip::showText(lineEditFamilyName->mapToGlobal(QPoint()), tr("Please Input a valid FamilyName"));
    }
    else
    {
        //QToolTip::showText(lineEditFamilyName->mapToGlobal(QPoint()), tr("It's Valid!"));
        QToolTip::hideText();
    }
    isModelRelevant();
}

void NewStudent2::on_lineEditName_text_changed(QString)
{
    if(lineEditName->hasAcceptableInput() == false)
    {
        QToolTip::showText(lineEditName->mapToGlobal(QPoint()), tr("Please Input a valid Name"));
    }
    else
    {

        //QToolTip::showText(lineEditName->mapToGlobal(QPoint()), tr("It's Valid!"));
        QToolTip::hideText();
    }
    isModelRelevant();
}

void NewStudent2::isModelRelevant()
{
    if(lineEditName->hasAcceptableInput() && lineEditFamilyName->hasAcceptableInput()
            && lineEditAge->hasAcceptableInput() && lineEditFakNumber->hasAcceptableInput())
    {
        btnSaveStudent->setDisabled(false);
    }else{
        btnSaveStudent->setDisabled(true);
    }
}
