#include "loadstudentsfrom.h"
#include <QDebug>
#include <QMessageBox>
#include <Serializers/downloadmanager.h>
#include <QtXml>

// FIXME: Downloadmanager
LoadStudentsFrom::LoadStudentsFrom(QWidget *parent)
    : QDialog( parent)
{
    //Grid

    radio1 = new QRadioButton(tr("&Local"));
    radio2 = new QRadioButton(tr("&Web"));
    radio1->setChecked(true);
    connect( radio2, &QRadioButton::toggled, this, &LoadStudentsFrom::onToggled);
    connect( radio1, &QRadioButton::toggled, this, &LoadStudentsFrom::onToggledRadio2);

    //Input
    inputUrl = new QLineEdit(this);
    inputUrl->setDisabled(true);
    //ToolButton
    btnInput = new QToolButton;
    btnInput->setText("...");
    connect(btnInput, SIGNAL(clicked()), this, SLOT(On_btnInput_clicked()));

    setLayouts();

}


void LoadStudentsFrom::LoadStudentsFromLocal()
{
     QString aFileName = QFileDialog::getOpenFileName(
               this,
               tr("Open File"),
               "C:/ProgramData/StudentsChanceryApp/config", "XmlFiles (*.xml);;DBFiles (*.db)");

     QFileInfo* studentsFilePath = new QFileInfo(aFileName);
     QString aFileExtension = studentsFilePath->suffix();

     //emit NotifyStudentsToRead(aFileName, aFileExtension);
     this->close();
     emit readStudentsFromFile(aFileName, aFileExtension);
     //m_serMng->readStudents(aFileName, aFileExtension);

}

void LoadStudentsFrom::LoadStudentsFromWeb()
{
      QString studentsUrl = inputUrl->text();
      QUrl aurl(studentsUrl);

      emit NotyfyFileToDownloadUrl(studentsUrl);

      emit executeDownload(studentsUrl);
      //QTimer *timer = new QTimer(this);
}

void LoadStudentsFrom::On_btnInput_clicked()
{
  if(radio1->isChecked())
  {
      LoadStudentsFromLocal();
  }
  if(radio2->isChecked())
  {
       LoadStudentsFromWeb();
  }
}

void LoadStudentsFrom::onToggled(bool checked)
{
    if(checked){
        inputUrl->setEnabled(true);
    }
}

void LoadStudentsFrom::onToggledRadio2(bool checked)
{
    if(checked){
        inputUrl->setDisabled(true);
    }
}

void LoadStudentsFrom::getStudentsDownloadedFromWeb(QString aFileName)
{
    QFileInfo* studentsFilePath = new QFileInfo(aFileName);
    QString aFileExtension = studentsFilePath->suffix();

    this->close();
    emit readStudentsFromFile(aFileName, aFileExtension);
}

void LoadStudentsFrom::setLayouts()
{
    grid = new QGridLayout;

    groupBox = new QGroupBox(tr("Choose source data"));
    vbox = new QVBoxLayout;
    vbox->addWidget(radio1);
    vbox->addWidget(radio2);
    vbox->addWidget(inputUrl);
    vbox->addWidget(btnInput);
    vbox->addStretch(1);
    groupBox->setLayout(vbox);

    grid->addWidget(groupBox);
    this->setLayout(grid);

}

