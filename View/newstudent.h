#ifndef NEWSTUDENT_H
#define NEWSTUDENT_H

#include <QDialog>
#include <QtDebug>
#include <QFileInfo>
#include <QtSql>
#include <QMessageBox>
#include <QSqlQuery>


namespace Ui {
class NewStudent;
}

class NewStudent : public QDialog
{
    Q_OBJECT

public:
    QSqlDatabase studentsDB;

public:
    explicit NewStudent(QWidget *parent = 0);
    ~NewStudent();

private slots:
    void on_btnSave_clicked();

    void on_pushButton_clicked();

private:
    Ui::NewStudent *ui;
};

#endif // NEWSTUDENT_H
