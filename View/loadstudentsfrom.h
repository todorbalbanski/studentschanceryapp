#ifndef LOADSTUDENTSFROM_H
#define LOADSTUDENTSFROM_H

#include <QWidget>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QLabel>
#include <QGroupBox>
#include <QDialog>
#include <QToolButton>
#include <QLineEdit>
#include <QList>
#include <QFileDialog>
#include <Serializers/sermng.h>

#include <QUrl>
#include <QFile>

class DownloadManager;

class LoadStudentsFrom : public QDialog
{
    Q_OBJECT

public:
    //LoadStudentsFrom( QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags(), SerMng &serMng);
    LoadStudentsFrom(QWidget *parent);
private:
    void LoadStudentsFromLocal();
    void LoadStudentsFromWeb();
    void setLayouts();

private:
    QRadioButton *radio1;
    QRadioButton *radio2;
    QLineEdit *inputUrl;
    QString typeToLoadTada;
    QToolButton *btnInput;

    QGridLayout *grid;
    QVBoxLayout *vbox;
    QGroupBox *groupBox;

    QFile* file;
    QLabel *chooseOption;
    QWidget *w;
public slots:
    void On_btnInput_clicked();
    void onToggled(bool checked);
    void onToggledRadio2(bool checked);
    void getStudentsDownloadedFromWeb(QString aFileName);

signals:
    void NotyfyFileToDownloadUrl(const QString aUrlPath);
    void readStudentsFromFile(QString aFileName, QString aFileExtension);
    void executeDownload(QString aStudentsUrl);
};

#endif // LOADSTUDENTSFROM_H
