#include "newstudent.h"
#include "ui_newstudent.h"
#include "mainview2.h"

NewStudent::NewStudent(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewStudent)
{
    ui->setupUi(this);


}

NewStudent::~NewStudent()
{
    delete ui;
}

void NewStudent::on_btnSave_clicked()
{
    QString name, familyName, fakNumber, address, age;
    name = ui->lineEdit_name->text();
    familyName = ui->lineEdit_familyname->text();
    fakNumber = ui->lineEdit_faknumber->text();
    address = ui->lineEdit_address->text();
    age = ui->lineEdit_age->text();
    if(!studentsDB.isOpen())
    {
//        connOpen();
    }

    QSqlQuery qry;
    qry.prepare("insert into Students(name, familyName, fakNumber, age, address) values ('"+name+"', '"+familyName+"', '"+fakNumber+"','"+age+"','"+address+"')");

    if(qry.exec())
    {
        QMessageBox::critical(this, tr("Save!"), tr("Student added!"));
//        connClose();
    }else{
        QMessageBox::critical(this, tr("Inventaire!"), qry.lastError().text());
    }
}

void NewStudent::on_pushButton_clicked()
{
    this->hide();
   // this->parentWidget()->show();
}
