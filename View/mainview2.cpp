#include "mainview2.h"
#include <QStandardItemModel>

#include "Model/studentmodelclass.h"
#include "newstudent2.h"
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QVariant>
#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

enum HeaderColumn {
    HeaderColumnId = 0,
    HeaderColumnName = 1,
    HeaderColumnFamilyName = 2,
    HeaderColumnFaknumber = 3,
    HeaderColumnAge = 4,
    HeaderColumnAddress= 5,
    HeaderColumnAvatar = 6
};

mainview2::mainview2(QWidget *parent)
    : QMainWindow(parent),
      m_studentEditor(nullptr),
      m_model( Q_NULLPTR ),
      m_quickview( Q_NULLPTR)
{
    //groupBox = new QGroupBox(tr("Choose search method"));
    m_radio1 = new QRadioButton(tr("&WildCard"));
    m_radio2 = new QRadioButton(tr("&RegularExpression"));
    m_radio1->setChecked(true);

    m_searchInput = new QLineEdit(this);
    connect(m_searchInput, SIGNAL(textChanged(const QString &)), this, SLOT(FilterTable(QString)));

    createTables();
    //
    m_buttonAddStudent = new QToolButton(nullptr);
    //Png
    m_buttonAddStudent->setIcon(QIcon(":/img/img/add.png"));
    m_buttonAddStudent->setIconSize(QSize(46,46));
    connect(m_buttonAddStudent, SIGNAL(clicked()), this, SLOT(on_btnAddStudent_clicked()));

    m_buttonRemoveStudent = new QToolButton(nullptr);
    m_buttonRemoveStudent->setIcon(QIcon(":/img/img/remove.png"));
    m_buttonRemoveStudent->setIconSize(QSize(46,46));
    m_buttonRemoveStudent->setDisabled(true);
    connect(m_buttonRemoveStudent, SIGNAL(clicked()), this, SLOT(on_btnRemoveStudent_clicked()));

    setConnections();
    setLayout();
    createActions();
    createMenus();

    // Create a widget
    w = new QWidget();
    w->setLayout(m_mainLayout);
    setCentralWidget(w);

}

mainview2::~mainview2()
{
    qDebug()<< "destruct object";
}

void mainview2::on_btnRemoveStudent_clicked()
{
    /*
    if(!Connection::GetDbInstance().isOpen())
    {
        Connection::connOpen();
    }

    Student student;
    bool isStudentDeleted = student.DeleteStudentFromDB(currentStudentId);
    if(isStudentDeleted)
    {
        QMessageBox::information(this, tr("Deleted!"), tr("Student Deleted !"));

    }else{
        QMessageBox::critical(this, tr("Inventaire!"), tr("There was an error!"));
    }
    Connection::connClose();
    */

}


void mainview2::on_tableView_activated(const QModelIndex &index)
{
    QModelIndex selectedStudent = m_tableview->currentIndex();
    int row = selectedStudent.row();
    m_currentStudentId = m_tableview->model()->data(m_tableview->model()->index(row,0) ).toString();
//    if(!Connection::GetDbInstance().isOpen())
//    {
//        Connection::connOpen();
//    }
    m_buttonRemoveStudent->setDisabled(false);

   //test
    // emit notifySelectedRowChanged(row);
}

void mainview2::onGuiSignal(const QVariant &qmlObject)
{
    auto obj = qmlObject.value<QObject*>();
    int currentRow = obj->property("choosedIndex").toInt();
    qDebug() <<"signal"<< obj->property("choosedIndex").toInt();

    QModelIndex newIndex = m_tableview2->model()->index(currentRow, 0);
    //m_tableview2->setSelectionBehavior(QAbstractItemView::SelectRows);
    //m_tableview->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableview2->setCurrentIndex(newIndex);
    m_tableview->setCurrentIndex(newIndex);

}

void mainview2::loadStudentsWindow()
{
    LoadStudentsFrom *m_loadStudents = new LoadStudentsFrom(this);
    m_loadStudents->setModal(true);
    connect(m_loadStudents, SIGNAL(readStudentsFromFile(QString,QString)), this, SLOT(readStudents(QString,QString)));
    connect(m_loadStudents, SIGNAL(NotyfyFileToDownloadUrl(QString)), this, SLOT(setUrlPathToDwnManager(QString)));
    connect(this, SIGNAL(DwnManagerNotifiedFileIsDownloaded(QString)), m_loadStudents, SLOT(getStudentsDownloadedFromWeb(QString)));
    connect(m_loadStudents, SIGNAL(executeDownload(QString)), this, SLOT(executeDownload(QString)));
    m_loadStudents->show();
}

void mainview2::saveStudents()
{
    const QString programDataDirPath = QStandardPaths::writableLocation( QStandardPaths::DataLocation );
    QString aFileName = QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                QString( "%1%2StudentsChanceryApp%2config" )
                .arg( programDataDirPath )
                .append( QDir::separator() ),
                "XmlFiles (*.xml);;DBFiles (*.db)");

    if(aFileName.isEmpty()){
        return;
    }
    QFileInfo* studentsFilePath = new QFileInfo(aFileName);
    QString aFileExtension = studentsFilePath->suffix();

    getTableToList();

    bool areStudentsSaved = saveStudentsSignal(m_model->m_studentsList, aFileName, aFileExtension);

    if(areStudentsSaved)
    {
        QMessageBox::information(this, tr("Saved!"), tr("Students Saved !"));

    }else{
        QMessageBox::critical(this, tr("Error!"), tr("There was an error!"));
    }
    //Connection::connClose();
}

void mainview2::tableViewSelectionChangedSlot(QModelIndex curIndex, QModelIndex prevIndex)
{
       int newRow = curIndex.row();
       int newCol = curIndex.column();
       QModelIndex newIndex = m_tableview2->model()->index(newRow, newCol);
       m_tableview2->setCurrentIndex(newIndex);
}

void mainview2::tableView2SelectionChangedSlot(QModelIndex curIndex, QModelIndex prevIndex)
{
        int newRow = curIndex.row();
        int newCol = curIndex.column();
        QModelIndex newIndex = m_tableview->model()->index(newRow, newCol);
        m_tableview->setCurrentIndex(newIndex);
}

void mainview2::getSelectedItem(int index)
{

}

void mainview2::tableSelectionStudentChanged(QModelIndex x , QModelIndex y)
{
    int selectedRow = x.row();
    m_selectionModel->currentRow = selectedRow;
    emit notifySelectedRowChanged(selectedRow);
}

void mainview2::FilterTable(const QString & aText)
{
    if(m_radio2->isEnabled())
    {
        proxyModel->setFilterRegExp(aText);
    }
    if(m_radio1->isEnabled())
    {
        proxyModel->setFilterWildcard(aText);
    }
    proxyModel->invalidate();
}

void mainview2::loadStudents(const QList<Student>& aStudentList)
{
    populateTableViewFromList(aStudentList);

}

void mainview2::readStudents(QString aFileName, QString aFileExtension)
{
    emit readStudentsFromFile(aFileName, aFileExtension);
}

void mainview2::setUrlPathToDwnManager(QString aUrlPath)
{
    emit setUrlPathToDwnMng(aUrlPath);
}

void mainview2::getStudentsDownloadedFromWeb(QString aFileName)
{
    emit DwnManagerNotifiedFileIsDownloaded(aFileName);
}

void mainview2::executeDownload(QString aStudentsUrl)
{
    emit ExecuteDownloading(aStudentsUrl);
}

void mainview2::createActions()
{
    m_LoadStudents = new QAction(tr("&Load..."), this);
    m_LoadStudents->setShortcuts(QKeySequence::New);
    m_LoadStudents->setStatusTip(tr("LoadFromXml"));
    connect(m_LoadStudents, &QAction::triggered, this, &mainview2::loadStudentsWindow);


    m_SaveStudents = new QAction(tr("&Save..."), this);
    m_SaveStudents->setStatusTip(tr("SaveToXml"));
    connect(m_SaveStudents, &QAction::triggered, this, &mainview2::saveStudents);

    m_ExitAct = new QAction(tr("&Exit"), this);
    m_ExitAct->setStatusTip(tr("Exit"));
    connect(m_ExitAct, &QAction::triggered, this, &mainview2::close);
    //connect(m_helper, SIGNAL(m_helper->NotifyLoadingStudentsSetted(WebOrLocalToLoadStudents);), this, SLOT(&mainview2::loadStudents(WebOrLocalToLoadStudents);));

}

void mainview2::createMenus()
{
    QMenu *m_fileMenu;

    m_fileMenu = menuBar()->addMenu(tr("&File"));
    m_fileMenu->addAction(m_LoadStudents);
    m_fileMenu->addAction(m_SaveStudents);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_ExitAct);
}

void mainview2::populateTableViewFromList(const QList<Student>& studentsList)
{
    m_model->setStudents(studentsList);
    proxyModel->setSourceModel(m_model);

    // FIXME: trybva modela da se pogriji za upovestyavaneto na view-tata che se e promenil
    m_tableview->updatesEnabled();


    //Resetting the model
    //m_tableview->setModel(m_model);
    setQML();
    //m_model->layoutChanged();

   // emit m_model->
}

void mainview2::loadStudentsLocal()
{
    // FIXME: ne izpolzvai zabiti putishta
    QString str = QProcess::systemEnvironment()[0];

    QString aFileName = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "C:/ProgramData/StudentsChanceryApp/config", "XmlFiles (*.xml);;DBFiles (*.db)");
    QFileInfo studentsFilePath = QFileInfo(aFileName);
    QString aFileExtension = studentsFilePath.suffix();

    emit readStudents(aFileName, aFileExtension);
}

void mainview2::setLayout()
{
    // Horizontal layout
    hLayout = new QHBoxLayout;

    QHBoxLayout* vLayoutUpper = new QHBoxLayout;
    vLayoutUpper->addWidget(m_radio1);
    vLayoutUpper->addWidget(m_radio2);
    vLayoutUpper->addWidget(m_searchInput);
    hLayout->addWidget(m_tableview);
    hLayout->addWidget(m_tableview2);
   //setQML();

    // Vertical layout with 2 buttons
    QHBoxLayout* vLayout = new QHBoxLayout;
    vLayout->addWidget(m_buttonAddStudent);
    vLayout->addWidget(m_buttonRemoveStudent);
    vLayout->addStretch();

    // Outer Layer
    m_mainLayout = new QVBoxLayout;
    // Add the previous two inner layouts
    m_mainLayout->addLayout(vLayoutUpper);
    m_mainLayout->addLayout(hLayout);
    m_mainLayout->addLayout(vLayout);

}

void mainview2::getTableToList()
{
    QAbstractItemModel* model = m_tableview->model();
    int rowCount = model->rowCount();
    for( int row = 0; row < rowCount; ++row )
    {
        Student student;
        student.setId(model->index(row,HeaderColumnId).data().value<int>());
        student.setName(model->index(row,HeaderColumnName).data().toString());
        student.setFamilyName(model->index(row,HeaderColumnFamilyName).data().toString());
        student.setAge(model->index(row,HeaderColumnAge).data().toString().toInt());
        student.setAddress(model->index(row,HeaderColumnAddress).data().toString());
        student.setFakNumber(model->index(row,HeaderColumnFaknumber).data().value<long>());
        student.setAvatar(model->index(row,HeaderColumnAvatar).data().toString());
        m_model->m_studentsList.append(student);
    }
}

void mainview2::setQML()
{
    //qDebug() << m_quickview;
    if ( m_quickview == Q_NULLPTR )
    {
        m_quickview = new QQuickView();
        m_quickview->setResizeMode(QQuickView::SizeRootObjectToView);
        QQmlContext *ctxt = m_quickview->rootContext();
        //mode
        m_selectionModel->setModel(proxyModel);
        ctxt->setContextProperty("myModel", proxyModel);
        ctxt->setContextProperty("selectionModel", m_selectionModel);


        QWidget *container = QWidget::createWindowContainer(m_quickview, this);
        container->setAutoFillBackground(true);

        container->setMinimumSize(660, 660);
        container->setMaximumSize(1800, 1800);
        container->setFocusPolicy(Qt::TabFocus);
        m_quickview->setSource((QUrl("qrc:/studentsGrid.qml")));

        QObject *rect = dynamic_cast<QObject*>(m_quickview->rootObject());
        connect(rect, SIGNAL(guiSignal(QVariant)), this, SLOT(onGuiSignal(QVariant)));

        hLayout->addWidget(container);
   }


        //view->setSource((QUrl("qrc:/StudentsQtQuick.qml")));
}

void mainview2::setConnections()
{
    connect(m_tableview->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(tableViewSelectionChangedSlot(QModelIndex,QModelIndex)));

    connect(m_tableview2->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(tableView2SelectionChangedSlot(QModelIndex,QModelIndex)));

    connect(m_selectionModel, SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(tableSelectionStudentChanged(QModelIndex,QModelIndex)));
}

void mainview2::createTables()
{
    m_tableview = new QTableView();
    m_tableview->setEditTriggers(QAbstractItemView::SelectedClicked);
    m_tableview->setSelectionBehavior(QAbstractItemView::SelectItems);

    m_tableview2 = new QTableView();
    m_tableview2->setEditTriggers(QAbstractItemView::SelectedClicked);
    m_tableview2->setSelectionBehavior(QAbstractItemView::SelectItems);

    connect(m_tableview, SIGNAL(activated(QModelIndex)),
            this, SLOT(on_tableView_activated(const QModelIndex)));

    m_model = new StudentsModel( this );

    //Sorting

    proxyModel = new ProxyModelForFiltering(nullptr);
    proxyModel->setSourceModel(m_model);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxyModel->setFilterKeyColumn(-1);


    m_selectionModel = new StudentsSelectionModel();
    m_selectionModel->setModel(proxyModel);

    //To be removed
    connect(this, SIGNAL(notifySelectedRowChanged(int)),
            proxyModel, SLOT(selectedRowChanged(int)));

    m_tableview->setModel( proxyModel );
    m_tableview->setSelectionModel(m_selectionModel);
    m_tableview->setSortingEnabled(true);
    m_tableview->sortByColumn(0, Qt::AscendingOrder);

    //#Fixme
    m_tableview2->setModel(proxyModel);
    m_tableview2->setSelectionModel(m_selectionModel);
    m_tableview2->setSortingEnabled(true);
    m_tableview2->sortByColumn(0, Qt::AscendingOrder);

}

#ifndef QT_NO_CONTEXTMENU
void mainview2::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu* menu = new QMenu(this);
    menu->exec(event->globalPos());
}
#endif // QT_NO_CONTEXTMENU

void mainview2::on_btnAddStudent_clicked()
{
    if ( m_studentEditor == Q_NULLPTR )
    {
        m_studentEditor = new NewStudent2( this );
        m_studentEditor->setModal(true);
    }
    m_studentEditor->show();
}

