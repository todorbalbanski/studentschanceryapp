#ifndef NEWSTUDENT2_H
#define NEWSTUDENT2_H

#include <QApplication>
#include <QDebug>
#include <QDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMessageBox>
#include <QSqlQuery>
#include <QToolButton>
#include <QtXml>
#include <QVBoxLayout>
#include "connection.h"
#include "Data/student.h"

class NewStudent2 : public QDialog
{
        Q_OBJECT
public:
    explicit NewStudent2( QWidget *parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags() );

private:
        //QSqlDatabase studentsDB;
        QHBoxLayout *hLayout;
        QVBoxLayout *vLayout;
        QVBoxLayout *mainLayout;

        //buttons
        QToolButton *btnSaveStudent;
        QToolButton *btnSaveXML;
        QToolButton *btnBack;

        //lineEdits
        QLineEdit *lineEditName;
        QLineEdit *lineEditFamilyName;
        QLineEdit *lineEditAge;
        QLineEdit *lineEditAddress;
        QLineEdit *lineEditFakNumber;

        //labels
        QLabel *labelName;
        QLabel *labelFamilyName;
        QLabel *labelAge;
        QLabel *labelAddress;
        QLabel *labelFakNumber;

        QWidget *w;

private:
        Student* student;

private:
        void setlayots();

signals:

private slots:

        void on_btnSaveStudent_clicked();

        void on_btnBack_clicked();

        void on_lineEditFakNumber_text_changed(QString);

        void on_lineEditAge_text_changed(QString);

        void on_lineEditFamilyName_text_changed(QString);

        void on_lineEditName_text_changed(QString);

        void isModelRelevant();

};

#endif // NEWSTUDENT2_H
