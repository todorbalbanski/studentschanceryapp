#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QMainWindow>
#include <QtDebug>
#include <QFileInfo>
#include <QtSql>
#include "newstudent.h"
#include "mainview.h"

namespace Ui {
class MainView;
}

class MainView : public QMainWindow
{
    Q_OBJECT

public:
    QSqlDatabase studentsDB;
    void connClose()
    {
        studentsDB.close();
        studentsDB.removeDatabase(QSqlDatabase::defaultConnection);
    }

    bool connOpen()
    {
        studentsDB = QSqlDatabase::addDatabase("QSQLITE");

        studentsDB.setDatabaseName("C:/ProgramData/LegrandGroup/HotelSupervisionServer/config/Students.db");
        studentsDB.open();

        if(studentsDB.isOpen())
        {
            return true;
        }

        return false;
     }
public:
    explicit MainView(QWidget *parent = 0);
    ~MainView();

private slots:
    void on_btnStudentSave_clicked();

    void on_btnStudentUpdate_clicked();

    void on_btnLoadTable_clicked();

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_btnDeleteStudent_clicked();

    void on_tableView_activated(const QModelIndex &index);

    void on_btnAddStudent_clicked();

private:
    QString currentStudentId;
    Ui::MainView *ui;
};

#endif // MAINVIEW_H
