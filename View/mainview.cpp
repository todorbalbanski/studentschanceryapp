#include "mainview.h"
#include "ui_mainview.h"
#include <QMessageBox>

MainView::MainView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainView)
{
    ui->setupUi(this);
    connOpen();
    if(!studentsDB.isOpen())
    {
        qDebug()<<"Failed to Open the DB!";
        return;
    }
    if(studentsDB.open())
    {
        ui->label_status->setText("Database is Connected!");
    }else{
        ui->label_status->setText("Failed to open Database!");
    }
}

MainView::~MainView()
{
    delete ui;
}

void MainView::on_btnStudentSave_clicked()
{

}

void MainView::on_btnStudentUpdate_clicked()
{
//    QString name, familyName, fakNumber, address, age;
//    name = ui->lineEdit_name->text();
//    familyName = ui->lineEdit_familyname->text();
//    fakNumber = ui->lineEdit_faknumber->text();
//    address = ui->lineEdit_address->text();
//    age = ui->lineEdit_age->text();
//    if(!studentsDB.isOpen())
//    {
//        connOpen();
//    }

//    QSqlQuery qry;
//    qry.prepare("Update Students set name='"+name+"', familyName='"+familyName+"', fakNumber='"+fakNumber+"',age= '"+age+"',address='"+address+"' where id='"+currentStudentId+"'");

//    if(qry.exec())
//    {
//        QMessageBox::critical(this, tr("Edit!"), tr("Student Edited !"));
//        connClose();
//    }else{
//        QMessageBox::critical(this, tr("Inventaire!"), qry.lastError().text());
//    }
}

void MainView::on_btnLoadTable_clicked()
{
    QSqlQueryModel * modal = new QSqlQueryModel();

    if(!studentsDB.isOpen())
    {
        connOpen();
    }

    QSqlQuery* qry = new QSqlQuery(studentsDB);
    qry->prepare("SELECT * FROM students");
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    ui->comboBox->setModel(modal);

    connClose();
    qDebug()<<(modal->rowCount());

}

void MainView::on_comboBox_currentIndexChanged(const QString &arg1)
{
//    currentStudentId = ui->comboBox->currentText();
//    if(!studentsDB.isOpen())
//    {
//        connOpen();
//    }

//    QSqlQuery qry;
//    qry.prepare("SELECT * FROM students where ID='"+currentStudentId+"'");

//    if(qry.exec())
//    {
//        while(qry.next())
//        {
//            ui->lineEdit_name->setText(qry.value(1).toString());
//            ui->lineEdit_familyname->setText(qry.value(2).toString());
//            ui->lineEdit_faknumber->setText(qry.value(3).toString());
//            ui->lineEdit_age->setText(qry.value(4).toString());
//            ui->lineEdit_address->setText(qry.value(5).toString());
//        }
//        connClose();
//    }else{
//    }

}

void MainView::on_btnDeleteStudent_clicked()
{
    if(!studentsDB.isOpen())
    {
        connOpen();
    }

    QSqlQuery qry;
    qry.prepare("Delete FROM Students where id='"+currentStudentId+"'");

    if(qry.exec())
    {
        QMessageBox::critical(this, tr("Deleted!"), tr("Student Deleted !"));
        connClose();
    }else{
        QMessageBox::critical(this, tr("Inventaire!"), qry.lastError().text());
    }
}

void MainView::on_tableView_activated(const QModelIndex &index)
{
    currentStudentId = ui->tableView->model()->data(index).toString();
    if(!studentsDB.isOpen())
    {
        connOpen();
    }
    qDebug()<<"this" + currentStudentId;
    QSqlQuery qry;
    qry.prepare("SELECT * FROM students where ID='"+currentStudentId+"'");

}

void MainView::on_btnAddStudent_clicked()
{
    //this->hide();
    NewStudent newstudent;
    newstudent.setModal(true);
    newstudent.exec();
}
