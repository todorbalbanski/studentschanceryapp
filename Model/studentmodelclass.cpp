#include "studentmodelclass.h"
#include <QHash>
#include <QByteArray>


StudentsModel::StudentsModel(QObject *parent)
    : QAbstractItemModel(parent)
{
//    QHash<int, QString> roles;
//    Student student;
//    roles[student.m_Id] = "id";
//    roles[student.m_Name] = "name";
//    roles[student.m_FamilyName] = "familyname";
//    roles[student.m_Age] = "age";
//    roles[student.m_Address] = "address";
//    roles[student.m_FakNumber] = "faknumber";
//    roles[student.m_Avatar] = "avatar";
}

StudentsModel::StudentsModel(const int &aId, const QString &aName, const QString &aFamilyName, const int &aAge, const QString aAddress, const long &aFakNumber, const QString &aAvatar)
{

}


void StudentsModel::setStudents(const QList<Student> &listStudents)
{
    m_studentsList = listStudents;
    //Notif
    //
}

int StudentsModel::rowCount(const QModelIndex &parent) const
{
    if (m_studentsList.size() == 0)
            return 0;
    return m_studentsList.count();
}

int StudentsModel::columnCount(const QModelIndex &parent) const
{
    int x = parent.isValid() ? 0 : ColumnIndex_Count;
    return parent.isValid() ? 0 : ColumnIndex_Count;
}

QVariant StudentsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
            return QVariant();

        if(index.row() >= m_studentsList.size() || index.row() < 0)
            return QVariant();

        if(role == Qt::DisplayRole || role == Qt::EditRole)
        {
            int indexRow = index.row();
            switch(index.column())
            {
                case ColumnIndex_Id:
                    return m_studentsList[indexRow].id();
                case ColumnIndex_Name:
                    return m_studentsList[indexRow].name();
                case ColumnIndex_FamilyName:
                    return m_studentsList[indexRow].familyName();
                case ColumnIndex_Age:
                    return m_studentsList[indexRow].age();
                case ColumnIndex_Address:
                    return m_studentsList[indexRow].address();
                case ColumnIndex_FakNumber:
                    return m_studentsList[indexRow].faknumber();
                case ColumnIndex_Avatar:
                    return m_studentsList[indexRow].avatar();
            }
        }


        if(role == IdRole || role == NameRole || role == FamilyNameRole || role == AgeRole || role == AddressRole || role == FakNumberRole || role == AvatarRole)
        {
            int indexRow = index.row();
            switch(role)
            {
                case IdRole:
                    return m_studentsList[indexRow].id();
                case NameRole:
                    return m_studentsList[indexRow].name();
                case FamilyNameRole:
                    return m_studentsList[indexRow].familyName();
                case AgeRole:
                    return m_studentsList[indexRow].age();
                case AddressRole:
                    return m_studentsList[indexRow].address();
                case FakNumberRole:
                    return m_studentsList[indexRow].faknumber();
                case AvatarRole:
                    return m_studentsList[indexRow].avatar();
            }
        }

        return QVariant();
}

QVariant StudentsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ( role != Qt::DisplayRole
         || orientation != Qt::Horizontal )
    {
        return QVariant();
    }

    switch ( section )
    {
        case 0:
            return tr("Id");
        case 1:
            return tr( "Name");
        case 2:
            return tr( "FamilyName");
        case 3:
            return tr( "Age");
        case 4:
            return tr( "Address");
        case 5:
            return tr( "FakNumber");
        case 6:
            return tr( "Avatar");
        default:
            break;
    }

    return QVariant();
}

bool StudentsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole && !(index.row() >= m_studentsList.size() || index.row() < 0))
        {
            int row = index.row();
            switch(index.column())
            {
            case 0:
                m_studentsList[row].setId(value.toInt());
                break;
            case 1:
                m_studentsList[row].setName(value.toString());
                break;
            case 2:
                m_studentsList[row].setFamilyName(value.toString());
                break;
            case 3:
                m_studentsList[row].setAge(value.toInt());
                break;
            case 4:
                 m_studentsList[row].setAddress(value.toString());
                break;
            case 5:
                 m_studentsList[row].setFakNumber(value.toString().toLong());
                break;
            case 6:
                 m_studentsList[row].setAvatar(value.toString());
                break;
            default:
                break;
            }
            emit dataChanged(index, index);
            return true;
        }
        return false;
}

QModelIndex StudentsModel::parent(const QModelIndex &child) const
{
    Q_UNUSED( child )

    return QModelIndex();
}

QModelIndex StudentsModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED( parent )

    return this->createIndex( row, column );
}

//void StudentsModel::dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
//{
//}

Qt::ItemFlags StudentsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
        flags |= (Qt::ItemIsEditable
                 |Qt::ItemIsSelectable
                 |Qt::ItemIsUserCheckable
                 |Qt::ItemIsEnabled
                 |Qt::ItemIsDragEnabled
                 |Qt::ItemIsDropEnabled);
        return flags;
}

QHash<int, QByteArray> StudentsModel::roleNames() const
{
        QHash<int, QByteArray> roles;
        roles[IdRole] = "id";
        roles[NameRole] = "name";
        roles[FamilyNameRole] = "familyname";
        roles[AddressRole] = "address";
        roles[AgeRole] = "age";
        roles[FakNumberRole] = "faknumber";
        roles[AvatarRole] = "avatar";
        return roles;
}

