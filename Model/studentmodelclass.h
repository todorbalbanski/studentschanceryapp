#ifndef STUDENTMODELCLASS_H
#define STUDENTMODELCLASS_H

#include <QAbstractItemModel>
#include "Data/student.h"
class StudentsModel : public QAbstractItemModel
{
  //  Q_OBJECT
public:
     StudentsModel(QObject *parent);
     StudentsModel(const int &aId, const QString &aName, const QString &aFamilyName, const int &aAge, const QString aAddress, const long &aFakNumber, const QString &aAvatar);
private:
    enum ColumnIndex
    {
        ColumnIndex_Id         = 0,
        ColumnIndex_Name       = 1,
        ColumnIndex_FamilyName = 2,
        ColumnIndex_Age        = 3,
        ColumnIndex_Address    = 4,
        ColumnIndex_FakNumber  = 5,
        ColumnIndex_Avatar     = 6,
        ColumnIndex_Count      = 7
    };

    enum AnimalRoles {
            IdRole             =Qt::UserRole +1,
            NameRole           ,
            FamilyNameRole     ,
            AgeRole             ,
            AddressRole         ,
            FakNumberRole       ,
            AvatarRole          ,
        };

public:
    void setStudents(const QList<Student> &listStudents);
    bool setData(const QModelIndex &index, const QVariant &value,
                     int role) Q_DECL_OVERRIDE;
    //void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>());
private:
        int columnCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
        int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
        QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
        QVariant headerData(int section, Qt::Orientation orientation,
            int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

        QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;
        QModelIndex index(int row, int column,
                          const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

        Qt::ItemFlags flags(const QModelIndex & index) const override;


protected:
        QHash<int, QByteArray> roleNames() const;

public:
        QList<Student> m_studentsList;

protected:

};

#endif // STUDENTMODELCLASS_H
