#ifndef PROXYMODELFORFILTERING_H
#define PROXYMODELFORFILTERING_H

#include <QSortFilterProxyModel>
#include <QObject>

class ProxyModelForFiltering : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ProxyModelForFiltering(QObject *parent = 0);


    //void setFilterKeyColumns(const QList<qint32> &filterColumns);
    //void addFilterFixedString(qint32 column, const QString &pattern);
protected:
  //  bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

private:
     QMap<qint32, QString> m_columnPatterns;

public slots:
        void selectedRowChanged(int rowChanged);

signals:
        void notifyQmlSelectedRowChanged(int indexToPass);

public slots:
};

#endif // PROXYMODELFORFILTERING_H
