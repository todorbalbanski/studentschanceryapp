#include "view/mainview2.h"
#include "Serializers/viewmanager.h"
#include <QApplication>

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ViewManager viewmanager;
    viewmanager.show();

//    QQuickView *view = new QQuickView();
//    view->setSource(QUrl(":/StudentsQtQuick.qml"));
//    view->show();
    const int res = a.exec();
    return res;
}
