#ifndef STUDENT_H
#define STUDENT_H
#include <QDialog>
#include <QtDebug>
#include <QSqlQuery>
#include <QString>
#include <QSqlTableModel>
#include <QDomDocument>
#include "connection.h"

class Student : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString familyName READ familyName WRITE setFamilyName NOTIFY familyNameChanged)
    Q_PROPERTY(int age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(long faknumber READ faknumber WRITE setFakNumber NOTIFY fakNumberChanged)
    Q_PROPERTY(QString avatar READ avatar WRITE setAvatar NOTIFY avatarChanged)
public:
    Student();
    Student( const Student& aOther );
    Student(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber, QString aAvatar);

    int id() const;
    QString name() const;
    QString familyName() const;
    int age() const;
    QString address() const;
    long faknumber() const;
    QString avatar() const;

    void setId(int aId);
    void setName(QString aName);
    void setFamilyName(QString aFamilyName);
    void setAge(int aAge);
    void setAddress(QString aAddress);
    void setFakNumber(long aFakNumber);
    void setAvatar(QString aAvatar);


    //Variables
private:
    int m_Id;
    QString m_Name;
    QString m_FamilyName;
    int m_Age;
    QString m_Address;
    long m_FakNumber;
    QString m_Avatar;

signals:
    void idChanged();
    void nameChanged();
    void familyNameChanged();
    void ageChanged();
    void addressChanged();
    void fakNumberChanged();
    void avatarChanged();


/*
private:
    //QSqlQuery* qry;
    QSqlTableModel *model;

public:
    bool SaveStudent(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber);
    bool SaveStudentToXml(QSqlTableModel *aModel);
    bool DeleteStudentFromDB(QString aCurrentStudentId);
*/
};

#endif // STUDENT_H
