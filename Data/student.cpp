#include "student.h"

Student::Student()
{
}

Student::Student(const Student &aOther)
    : m_Id( aOther.m_Id )
    , m_Name( aOther.m_Name )
    , m_FamilyName( aOther.m_FamilyName )
    , m_Age( aOther.m_Age )
    , m_Address( aOther.m_Address )
    , m_FakNumber( aOther.m_FakNumber )
    , m_Avatar (aOther.m_Avatar)
{
}

Student::Student(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber, QString aAvatar)
{
    m_Name= aName;
    m_FamilyName = aFamilyName;
    m_Age = aAge;
    m_Address = aAddress;
    m_FakNumber = aFakNumber;
    m_Avatar = aAvatar;
}

int Student::id() const
{
    return m_Id;
}

QString Student::name() const
{
    return m_Name;
}

QString Student::familyName() const
{
    return m_FamilyName;
}

int Student::age() const
{
    return m_Age;
}

QString Student::address() const
{
    return m_Address;
}

long Student::faknumber() const
{
    return m_FakNumber;
}

QString Student::avatar() const
{
    return m_Avatar;
}

void Student::setId(int aId)
{
    if (aId != m_Id) {
            m_Id = aId;
            emit idChanged();
    }
}

void Student::setName(QString name)
{
    if (name != m_Name) {
            m_Name = name;
            emit nameChanged();
    }
}

void Student::setFamilyName(QString aFamilyName)
{
    if (aFamilyName != m_FamilyName) {
            m_FamilyName = aFamilyName;
            emit familyNameChanged();
    }
}

void Student::setAge(int aAge)
{
    if (aAge != m_Age) {
            m_Age = aAge;
            emit ageChanged();
    }
}

void Student::setAddress(QString aAddress)
{
    if (aAddress != m_Address) {
            m_Address = aAddress;
            emit addressChanged();
    }
}

void Student::setFakNumber(long aFakNumber)
{
    if (aFakNumber != m_FakNumber) {
            m_FakNumber = aFakNumber;
            emit fakNumberChanged();
    }
}

void Student::setAvatar(QString aAvatar)
{
    if (aAvatar != m_Avatar) {
            m_Avatar = aAvatar;
            emit avatarChanged();
        }
}

/*
bool Student::SaveStudent(QString aName, QString aFamilyName, int aAge, QString aAddress, long aFakNumber)
{
    Student student;
    student.Name = aName;
    student.FamilyName = aFamilyName;
    student.FakNumber = aFakNumber;
    student.Address = aAddress;
    student.Age = aAge;

    if(!Connection::GetDbInstance().isOpen())
    {
        Connection::connOpen();
    }

    QString FakNumberString = QString::number(student.FakNumber);
    QString AgeString = QString::number(aAge);
    QSqlQuery qry;
    qry.prepare("insert into Students(name, familyName, fakNumber, age, address) values ('"+student.Name+"', '"+student.FamilyName+"', '"+FakNumberString+"','"+AgeString+"','"+student.Address+"')");

    if(qry.exec())
    {
        Connection::connClose();
        return true;

    }else{
        Connection::connClose();
        return false;
    }

}

bool Student::SaveStudentToXml(QSqlTableModel* aModel)
{
    model = aModel;
    //Create XmlDocument
    QDomDocument document;

    //Make the root Element
    //if(document)
    QDomElement root = document.createElement("students");

    //Add it to the document
    document.appendChild(root);

    int row = model->rowCount();
    int col = model->columnCount();
    for(int i=0; i< row ;i++)
    {
        QString currentStudentID = model->data(model->index(i, 0), Qt::DisplayRole).toString();
        QString currentStudentName = model->data(model->index(i, 1), Qt::DisplayRole).toString();
        QString currentStudentFamilyName = model->data(model->index(i, 2), Qt::DisplayRole).toString();
        QString currentStudentFaknumber = model->data(model->index(i, 3), Qt::DisplayRole).toString();
        QString currentStudentAge = model->data(model->index(i, 4), Qt::DisplayRole).toString();
        QString currentStudentAddress = model->data(model->index(i, 5), Qt::DisplayRole).toString();

        QDomElement student = document.createElement("student");
        QDomElement id = document.createElement("id");
        QDomText idText = document.createTextNode(currentStudentID);
        id.appendChild(idText);
        student.appendChild(id);

        QDomElement name = document.createElement("name");
        QDomText nameText = document.createTextNode(currentStudentName);
        name.appendChild(nameText);
        student.appendChild(name);

        QDomElement familyname = document.createElement("familyname");
        QDomText familyNameText = document.createTextNode(currentStudentFamilyName);
        familyname.appendChild(familyNameText);
        student.appendChild(familyname);

        QDomElement faknumber = document.createElement("faknumber");
        QDomText faknumberText = document.createTextNode(currentStudentFaknumber);
        faknumber.appendChild(faknumberText);
        student.appendChild(faknumber);

        QDomElement age = document.createElement("age");
        QDomText ageText = document.createTextNode(currentStudentAge);
        age.appendChild(ageText);
        student.appendChild(age);

        QDomElement address = document.createElement("address");
        QDomText addressText = document.createTextNode(currentStudentAddress);
        address.appendChild(addressText);
        student.appendChild(address);

        root.appendChild(student);

    }

    //Write to file
    //QFile file( QStandardPaths::writableLocation( QStandardPaths::DesktopLocation ) % QDir::separator() % "StudentsFromDB.xml" );
    QFile file( "C:/ProgramData/StudentsChanceryApp/config/StudentsFromDB.xml" );
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Failed to Open the file";
        return false;
    }else{
        QTextStream stream(&file);
        stream << document.toString();
        file.close();
        return true;
    }
}

bool Student::DeleteStudentFromDB(QString aCurrentStudentId)
{
    QSqlQuery qry;
    qry.prepare("Delete FROM Students where id='"+aCurrentStudentId+"'");
    if(qry.exec())
    {
        return true;
    }else{
        return false;
    }

}
*/
