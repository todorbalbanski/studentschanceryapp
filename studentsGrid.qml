import QtQuick 2.3

Item{
    id: itemId
    width: 500
    height: 500
    property alias gridView: gridView
    property alias itemId: itemId
    scale: 1.1
    opacity: 1

    signal guiSignal(variant objQml);

    // QtObject {
    //     id:  selectionId
    //     property int choosedRow: selectionModel.currentIndex.row;
    // }
    //
    // Connections{
    //     target: myModel
    //     onNotifyQmlSelectedRowChanged: selectItem(indexToPass);
    //
    // }

    // function selectItem(indexToPass){
    //     if(!indexToPass) { return }
    //     console.log("through model" + selectionId.choosedRow)
    //     gridView.currentIndex = selectionId.choosedRow;
    // }


    Component {
            id: highlightBar
            Rectangle {
                width: 200; height: 200
                color: "grey"
                y: gridView.currentItem.y;
                x: gridView.currentItem.x;
                Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
                Behavior on x { SpringAnimation { spring: 2; damping: 0.1 } }
              }
        }

    GridView {
        id: gridView
        x: 67
        y: 43
        width: 500
        height: 500
        model: myModel
        cellHeight: 200
        cellWidth: 200
        currentIndex: {
            console.log(selectionModel.currentIndex.row)
            console.log( "setting grid view index" )
            return selectionModel.currentIndex.row
        }
        onActiveFocusChanged: {
            //console.log( "aaaaaaaaaaaaa" )
            //return selectionModel.currentIndex.row
//            console.log( "setting selection model index" )
//            console.log(currentIndex);

//            console.log(itemId.index);
           // selectionModel.setSelectionChanged(gridView.currentIndex);
        }
        delegate: Column {
                spacing: 5
                StudentsField{

                }

            }
        highlight:  highlightBar
        focus: true
        highlightFollowsCurrentItem: false

    }
}
